import React, {Component} from 'react';
import {newsArr} from './newsArr'


class ReverseButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: newsArr
        }
    }

    render() {
        return (
            <button onClick={this.revertNews}>Reverse</button>
        )
    }

    revertNews = () => {
        this.setState({
            news: this.state.news.reverse()
        })
    }
}

export default ReverseButton;
