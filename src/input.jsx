import React, { Component } from 'react'
import News from './newsClass';
import { newsArr } from './newsArr'

class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            news: newsArr,
            input: ""
        }
    }

    handleChange = (event) => {
        this.setState({ input: event.target.value });
    }

    searchNews = () => {
        this.setState({ news: this.state.news.filter((post) => { return post.title === this.state.input }) })
    }

    render() {
        const postsList = this.state.news.map(post => <News key={post.id} post={post}/>)
        return (
            <>
                <input className="searchInput" type="text" placeholder="Введите название новости" value={this.state.input}
                    onChange={this.handleChange} />

                <button className="searchBtn" onClick={this.searchNews}>Найти</button>
                {postsList}
            </>
        )
    }

}

export default Input;
