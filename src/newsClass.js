import React, {Component} from 'react';

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: this.props.flag
        }

    }

    componentWillReceiveProps(nextProps, nextContext){
        if(nextProps.flag !== this.state.isOpen){
            this.setState({isOpen: nextProps.flag})
        }
    }

    handleClick = ()=> {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    render() {
        
        let info = (
            <div>
                <p>{this.props.post.content}</p>
            </div>
        );

        return (
            <div>
                <h2>{this.props.post.title}</h2>
                <img src={this.props.post.picture} alt={this.props.post.title}/>
                <h4>{this.props.post.description}</h4>
                <button onClick={this.handleClick}>{this.state.isOpen ? "Свернуть" : "Развернуть"}</button>
                {this.state.isOpen && info}
            </div>
        )
    }
}

export default News;
