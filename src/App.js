import React, {Component} from 'react'
import './App.css';
import {newsArr} from './newsArr'
import News from './newsClass';
// import Input from './input'
// import ReverseButton from './reverseButton';

class App extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            news: newsArr,
            input: ""
        }
  }

  render() {

    let postsList = this.state.news.map((post, i) => i === 0 ? <News key={post.id} post={post} flag/> : <News key={post.id} post={post} />)
    return (
        <>
            {/* <ReverseButton/> */}
            {/* <Input/> */}
            <button className="revertBtn" onClick={this.revertNews}>Revert</button>
            <input className="searchInput" type = "text" placeholder="Search news" value = {this.state.input} 
            onChange = {this.handleChange}/>
            <button className = "searchBtn" onClick = {this.searchNews}>Find</button>
            <button className = "resetBtn" onClick = {this.resetNews}>Reset</button>
            {postsList}
        </>
    )
  }
    revertNews = () => {
        this.setState({
            news: this.state.news.reverse()
        })
    }
    resetNews = () => {
        this.setState({
            news: newsArr
        })
    }
    handleChange = (event) => {
        this.setState({input: event.target.value});
      }
    searchNews = () => {
        this.setState({
            news: this.state.news.filter(
                (post) => {return post.title === this.state.input}
            )
        })
    }

}

export default App;
